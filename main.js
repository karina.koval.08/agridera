var app = new Value ({
    el:"article",
    data:{
        products: [{id:1, title: "Prod 1", short_text:'short_text', image:'1.png', desc:"Full desc"},
    {id:2, title: "Prod 2", short_text:"short_text", image:'2.png', desc:"Full desc"},
    {id:3, title:"Prod 3", short_text:"short_text", image:'3.png', desc:"Full desc"},
    {id:4, title:"Prod 4", short_text:"short_text", image:'4.png', desc:"Full desc"},
    {id:5, title:"Prod 5", short_text:"short_text", image:'5.png', desc:"Full desc"}]
},
mounted: function(){
    console.log(window.localStorage.getItem('prod'));
},
methods: {
    addItem:function(id){
    window.localStorage.setItem('prod', id);
    }
    
},
product: [],
btnVisible: false,
cart: [],
contactFields: [],
order: 0,
},
mounted: function () {
this.getProduct();
},
methods: {
    getProduct: function () {
        if(window.location.hash){
            var id = window.location.hash.replace("#", "");
            if(this.products && this.products.length>0){      
      for (i in this.products) {
        if (this.products[i] && this.products[i].id && id==this.products[i].id) this.products=this.products[i];
      }
    }
}addToCart: function (id) {
    var cart = [];
    if (window.localStorage.getItem("cart")) {
      cart = window.localStorage.getItem("cart").split(",");
    }
    if (cart.indexOf(String(id))) {
      cart.push(id);
      window.localStorage.setItem("cart", cart.join());
      this.btnVisible = true;
    }
}
  },
  checkInCart() {
    if (
      this.product &&
      this.product.id &&
      window.localStorage
        .getItem("cart")
        .split(",")
        .indexOf(String(this.product.id)) != -1
    )
      this.btnVisible = true;
  },
  getCart() {
    if (window.localStorage.getItem("cart") != null) {
      if (this.products != null && this.products.length > 0) {
        for (let i in this.products) {
          if (
            this.products[i] != null &&
            this.products[i].id != null &&
            window.localStorage
              .getItem("cart")
              .split(",")
              .indexOf(String(this.products[i].id)) != -1
          )
            this.cart.push(this.products[i]);
        }
      }
    }
  },
  removeFromCart(id) {
    let cart = [];
    if (window.localStorage.getItem("cart") != null) {
      cart = window.localStorage.getItem("cart").split(",");
    }
    if (cart.indexOf(String(id)) != -1) {
      cart.splice(cart.indexOf(String(id)), 1);
      window.localStorage.setItem("cart", cart.join(","));
      this.cart = [];
      this.getCart();
    }
  },
  makeOrder() {
    this.cart = [];
    window.localStorage.setItem("cart", "");
    this.order = 1;
  },
},
);
